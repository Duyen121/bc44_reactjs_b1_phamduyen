import logo from "./logo.svg";
import "./App.css";
import Header from "./Ex_Layout_Component/Header";
import Banner from "./Ex_Layout_Component/Banner";
import Item from "./Ex_Layout_Component/Item";
import Footer from "./Ex_Layout_Component/Footer";

function App() {
  return (
    <div className="layout-container">
      <Header />
      <div className="body-content">
        <Banner />
        <Item />
      </div>
      <Footer />
    </div>
  );
}

export default App;
