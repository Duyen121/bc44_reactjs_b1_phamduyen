import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer py-5 bg-dark">
        <div className="container">
          <p className="m-0 text-center text-white">
            Copyright &copy; Your Website 2023
          </p>
        </div>
      </div>
    );
  }
}
